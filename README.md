# force_sensor

This is a ROS2 package which handles the force sensor readings and publishes them in real time to the ros topic.
It publishes the topics `geometry_msgs::WrenchStamped`:
 - `wrench_stream` 


The package has parameters are:
- `sensor_ip`
- `sensor_scale_factor` - default 1000000 - outputting force in N
- `sensor_freq` - default 60hz
- `debug` - if `True` the node will dump all the forces and moments to the terminal
- `sensor_frame` - frame where the sensor is placed (default `world`)
- `world_frame` - base frame where `wrench_stream` is expressed (default `world`)
- `handle_mass` - mass of the handle of the sensor (if applicable) default `0.0`
can be found in the `config/ft.yaml` file.

## Installation
To test the code, place this package in your ros2 worskpace. 

```sh
cd my/workspace/path
cd src
git clone git@gitlab.inria.fr:auctus-team/people/antunskuric/ros2/packages/force_sensor.git
cd ..
```

Build the workspace
```sh
colcon build
source install/setup.bash # source the workspace 
```

Launch 
```sh
source install/setup.bash
ros2 launch force_sensor display.launch.py 
```


<img src="https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/force_sensor/-/raw/main/rviz.png">

## Package data workflow 
 The package workflow is shown on this figure 
 
 <img src="https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/force_sensor/-/raw/main/diagram.jpg">


This package is a fork of an old repo: https://gitlab.inria.fr/auctus/force_torque_sensor