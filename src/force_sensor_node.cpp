//    includes
#include "std_msgs/msg/string.hpp"
#include "FTDriver.hpp"
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/vector3_stamped.hpp>
#include <geometry_msgs/msg/wrench_stamped.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2/exceptions.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/buffer.h>

#include <sstream>
#include <iostream>
#include "rclcpp/rclcpp.hpp"

//    constants
#define MESSAGE_QUEUE 10.0
#define LOOP_RATE_DEFAULT 100.0
#define SCALE_FACTOR_DEFAULT 1000000.0
#define SENSOR_IP_ADDRESS_DEFAULT "172.16.0.11"
#define SENSOR_TF_FRAME_DEFAULT "world"
#define BASE_TF_FRAME_DEFAULT "world"

#include "UDP.hpp"


// node process
int main(int argc, char **argv)
{
    // initialize node and message topics
    rclcpp::init(argc, argv);
    auto n = rclcpp::Node::make_shared("force_sensor_node");
    
    
    std::string sensor_ip_address, sensor_TF,base_TF;
    double sensor_scale_factor, loop_rate;
    bool debugging;
    double handle_mass;

    n->declare_parameter("sensor_ip_address",SENSOR_IP_ADDRESS_DEFAULT);
    n->declare_parameter("sensor_scale_factor",SCALE_FACTOR_DEFAULT);
    n->declare_parameter("loop_rate",LOOP_RATE_DEFAULT);
    n->declare_parameter("debug",0);
    n->declare_parameter("world_frame",BASE_TF_FRAME_DEFAULT);
    n->declare_parameter("sensor_frame",SENSOR_TF_FRAME_DEFAULT);
    n->declare_parameter("handle_mass", 0.0);

    sensor_scale_factor = n->get_parameter("sensor_scale_factor").as_double();
    sensor_ip_address = n->get_parameter("sensor_ip_address").as_string();
    loop_rate = n->get_parameter("loop_rate").as_double();
    debugging = n->get_parameter("debug").as_int();
    handle_mass = n->get_parameter("handle_mass").as_double();
    sensor_TF = n->get_parameter("sensor_frame").as_string();
    base_TF = n->get_parameter("world_frame").as_string();

    RCLCPP_INFO_STREAM(n->get_logger(),"Sensor ip: " << sensor_ip_address);
    RCLCPP_INFO_STREAM(n->get_logger(),"Sensor scale factor: " << sensor_scale_factor);
    RCLCPP_INFO_STREAM(n->get_logger(),"Sensor frequency: " << loop_rate << "Hz");
    RCLCPP_INFO_STREAM(n->get_logger(),"Debuging: " << debugging);
    RCLCPP_INFO_STREAM(n->get_logger(),"Sensor TF: " << sensor_TF);
    RCLCPP_INFO_STREAM(n->get_logger(),"Base TF: " << base_TF);
    RCLCPP_INFO_STREAM(n->get_logger(),"Handle mass: " << handle_mass << "kg");
       
    rclcpp::Rate loop(loop_rate);

    // innitialize messages
    auto wrench_pub = n->create_publisher<geometry_msgs::msg::WrenchStamped>("wrench_stream", MESSAGE_QUEUE);
    geometry_msgs::msg::WrenchStamped wrench_msg;
    wrench_msg.header.frame_id = base_TF;
    
    // create socket
    FTDriver sensor(sensor_ip_address);
    sensor.start();

    std::shared_ptr<tf2_ros::Buffer> tf_buffer;
    std::shared_ptr<tf2_ros::TransformListener> listener;
    tf_buffer.reset(new tf2_ros::Buffer(n->get_clock()));
    listener.reset(new tf2_ros::TransformListener(*tf_buffer));

    geometry_msgs::msg::TransformStamped transform_msg;
    tf2::Transform transform;

    bool found_tf = false;
    long int tf_trys_count = 0;
    while((!found_tf) && (tf_trys_count < 20)){
        tf_trys_count++;
        try{
            transform_msg = tf_buffer->lookupTransform(
              base_TF,
              sensor_TF,
              tf2::TimePointZero);
            tf2::fromMsg(transform_msg.transform,transform);
            found_tf = true;
        }
        catch (tf2::TransformException & ex){ 
            // do nothing - wait
        }
        RCLCPP_WARN(n->get_logger(),"Still waiting transform_msg data - sleep 2s!");
        rclcpp::sleep_for(std::chrono::seconds(2));
    }   
    if(!found_tf){
        RCLCPP_ERROR(n->get_logger(),"TF data not received - quitting");
        return -1;
    }

    RCLCPP_INFO(n->get_logger(),"Callibration starting ");
    FTDriver::Record offset = FTDriver::Record();
    int num_mes = 0;
    
    tf2::Vector3 handle_g(0, 0, handle_mass*9.81);
    transform_msg = tf_buffer->lookupTransform(
              sensor_TF,
              base_TF,
              tf2::TimePointZero);
    tf2::fromMsg(transform_msg.transform,transform);
    // transformation to base cs
    tf2::Vector3  g = transform.getBasis()*handle_g*sensor_scale_factor;

    while(num_mes < 10){ // dont put more than 100 - overflow int
        // get latest message
        sensor.start();
        
        FTDriver::Record r_tmp = sensor.get();
        if(r_tmp.RDTSequence ){
            offset = offset + r_tmp;
            offset.Fx = offset.Fx + g[0];
            offset.Fy = offset.Fy + g[1];
            offset.Fz = offset.Fz + g[2];
            num_mes++;
        }
    }
    offset = offset/num_mes;
    if(debugging)
            std::cout << offset.Fx << ' ' << offset.Fy << ' ' << offset.Fz << ' ' << offset.Tx << ' ' << offset.Ty << ' ' << offset.Tz << std::endl;

    RCLCPP_INFO(n->get_logger(),"Callibration done." );

    // infinit loop
    while (rclcpp::ok())
    {
        // get latest message
        sensor.start();
        FTDriver::Record r = sensor.getLatest() - offset;
        
        // transform the wrench
        tf2::Vector3 f,t;
        f = tf2::Vector3(r.Fx,r.Fy,r.Fz)/sensor_scale_factor;
        t = tf2::Vector3(r.Tx,r.Ty,r.Tz)/sensor_scale_factor;
        
        // transfrom if necessary
        if (base_TF != sensor_TF){
            try{
                transform_msg = tf_buffer->lookupTransform(
                          base_TF,
                          sensor_TF,
                          tf2::TimePointZero);
                tf2::fromMsg(transform_msg.transform,transform);
                // transformation to base cs
                f = transform.getBasis()*f + handle_g;
                t = transform.getBasis()*t;
            }
            catch (tf2::TransformException & ex){ 
              RCLCPP_ERROR(n->get_logger(),"%s",ex.what());
            }
        }

        // prepare messages
        wrench_msg.wrench.force.x = f[0];
        wrench_msg.wrench.force.y = f[1];
        wrench_msg.wrench.force.z = f[2];
        wrench_msg.wrench.torque.x = t[0];
        wrench_msg.wrench.torque.y = t[1];
        wrench_msg.wrench.torque.z = t[2];
        wrench_msg.header.stamp = n->get_clock()->now();
        wrench_pub->publish( wrench_msg );
        
        if(debugging)
            std::cout << wrench_msg.wrench.force.x << ' ' << wrench_msg.wrench.force.y << ' ' << wrench_msg.wrench.force.z << ' ' << wrench_msg.wrench.torque.x << ' ' << wrench_msg.wrench.torque.y << ' ' << wrench_msg.wrench.torque.z << std::endl;

        // end
        rclcpp::spin_some(n);
        loop.sleep();
    }
    return 0;
}
