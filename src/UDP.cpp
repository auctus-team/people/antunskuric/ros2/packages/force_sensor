#include "UDP.hpp"

#include <string.h>
#include <stdlib.h>
#include <iomanip>
#include <chrono>

#define UDP_RCV_BUFFERSIZE 10000
#define UDP_TIMEOUT_SEC 0
#define UDP_TIMEOUT_USEC 10

#define ERROR ("\033[0;31mERROR\033[0m")
#define WARNING ("\033[0;33mWARNING\033[0m")
#define SUCCESS ("\033[0;32mSUCCESS\033[0m")
#define INFOS ("\033[0;36mINFOS\033[0m")


/// Default
UDPsocket::UDPsocket(const int& port) : timestamp(0), errors(0)
{
    //  create socket
    rcvBuffer = new uint8_t[UDP_RCV_BUFFERSIZE];
    udpsocket = socket(AF_INET, SOCK_DGRAM, 0);

    sockaddr_in address;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    address.sin_family = AF_INET;
    if (bind(udpsocket, reinterpret_cast<sockaddr*>(&address), sizeof(address)) != 0)
        std::cout << ERROR << " : UDPsocket : opening UDP socket fail" << std::endl;

    struct timeval read_timeout;
    read_timeout.tv_sec = UDP_TIMEOUT_SEC;
    read_timeout.tv_usec = UDP_TIMEOUT_USEC;
    setsockopt(udpsocket, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
}
UDPsocket::~UDPsocket()
{
    close(udpsocket);
    delete[] rcvBuffer;
}
//


/// Communication functions
bool UDPsocket::sendMessageTo(const uint8_t* msg, const unsigned short& msgSize, const char* destination, const char* port)
{
    //  compute destination
    sockaddr_storage addrDest = {};
    if (resolveDestination(destination, AF_INET, port, &addrDest) != 0)
    {
        std::cout << ERROR << " : UDPsocket : destination address configuration fail" << std::endl;
        return false;
    }

    //  send message
    if (sendto(udpsocket, msg, msgSize, 0, reinterpret_cast<const sockaddr*>(&addrDest), sizeof(addrDest)) < 0)
    {
        switch (errno)
        {
            case EACCES:       std::cout << ERROR << " : UDPsocket : access denied" << std::endl; break;
            case EBADF:        std::cout << ERROR << " : UDPsocket : invalid descriptor" << std::endl; break;
            case ECONNRESET:   std::cout << ERROR << " : UDPsocket : connection reset" << std::endl; break;
            case EDESTADDRREQ: std::cout << ERROR << " : UDPsocket : no address set" << std::endl; break;
            case EFAULT:       std::cout << ERROR << " : UDPsocket : invalid address or argument" << std::endl; break;
            case EINTR:        std::cout << ERROR << " : UDPsocket : interrupt occur" << std::endl; break;
            case EINVAL:       std::cout << ERROR << " : UDPsocket : invalid argument" << std::endl; break;

            case EISCONN:    std::cout << ERROR << " : UDPsocket : ?EISCONN?" << std::endl; break;
            case EMSGSIZE:   std::cout << ERROR << " : UDPsocket : message size problem(" << msgSize << "o)" << std::endl; break;
            case ENOBUFS:    std::cout << ERROR << " : UDPsocket : network congestion" << std::endl; break;
            case ENOMEM:     std::cout << ERROR << " : UDPsocket : no memory available" << std::endl; break;
            case ENOTCONN:   std::cout << ERROR << " : UDPsocket : socket not connected" << std::endl; break;
            case ENOTSOCK:   std::cout << ERROR << " : UDPsocket : argument is not a socket" << std::endl; break;
            case EOPNOTSUPP: std::cout << ERROR << " : UDPsocket : flags not supported" << std::endl; break;

            case EPIPE: std::cout << ERROR << " : UDPsocket : ?EPIPE?" << std::endl; break;
            default: std::cout << ERROR << " : UDPsocket : unknown" << std::endl; break;
        }
        errors++;
        return false;
    }
    else
    {
        timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        return true;
    }
}
std::vector<uint8_t> UDPsocket::read()
{
    std::vector<uint8_t> message;
    sockaddr_in src_addr;
    socklen_t src_addr_len = sizeof(src_addr);
    int rcvlen = recvfrom(udpsocket, rcvBuffer, UDP_RCV_BUFFERSIZE, MSG_DONTWAIT, reinterpret_cast<sockaddr*>(&src_addr), &src_addr_len);
    if(rcvlen < 0)
    {
        switch (errno)
        {
            case EBADF:       std::cout << ERROR << " : UDPsocket : bad file descriptor" << std::endl; break;
            case ECONNRESET:  std::cout << ERROR << " : UDPsocket : connection closed by peer" << std::endl; break;
            case EINTR:       std::cout << ERROR << " : UDPsocket : signal interrupt" << std::endl; break;
            case EINVAL:      std::cout << ERROR << " : UDPsocket : MSG_OOB flag set and no out-of-band data available" << std::endl; break;
            case ENOTCONN:    std::cout << ERROR << " : UDPsocket : socket not in connected state" << std::endl; break;
            case ENOTSOCK:    std::cout << ERROR << " : UDPsocket : socket argument does not refer to a socket" << std::endl; break;
            case EOPNOTSUPP:  std::cout << ERROR << " : UDPsocket : flag not supported" << std::endl; break;
            case ETIMEDOUT:   std::cout << ERROR << " : UDPsocket : timeout" << std::endl; break;
            case EIO:         std::cout << ERROR << " : UDPsocket : I/O error while reading" << std::endl; break;
            case ENOBUFS:     std::cout << ERROR << " : UDPsocket : insufficient resources available" << std::endl; break;
            case ENOMEM:      std::cout << ERROR << " : UDPsocket : insufficient memory available" << std::endl; break;
            default: errors--; break;
        }
		errors++;
    }
    else
    {
		timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		message = std::vector<uint8_t>(rcvBuffer, rcvBuffer + rcvlen);
    }
    return message;
}
//

/// Set / get functions
#define LAST_UNREACH -1
#define LAST_SUCCESS 1
bool UDPsocket::getIpFromHostname(const std::string& hostname, std::string* ip, const int& lastState)
{
    *ip = "";
    hostent* host = gethostbyname(hostname.c_str());
    if (host == NULL)
    {
        if(lastState != LAST_UNREACH)
            std::cout << ERROR << " : UDPsocket : host unreachable" << std::endl;
        return false;
    }

    in_addr* address = (in_addr*)host->h_addr;
    *ip = inet_ntoa(*address);
    if(lastState != LAST_SUCCESS)
        std::cout << SUCCESS << " : UDPsocket : host " << hostname << " found at " << *ip << std::endl;

    return true;
}
unsigned long UDPsocket::getErrorsCount() const { return errors; }
unsigned long UDPsocket::getTimestamp() const { return timestamp; }
//


/// Protected functions
int UDPsocket::resolveDestination(const char* destination, int family, const char* port, sockaddr_storage* pAddr)
{
    int result;
    addrinfo* result_list = NULL;
    addrinfo hints = {};
    hints.ai_family = family;
    hints.ai_socktype = SOCK_DGRAM;
    result = getaddrinfo(destination, port, &hints, &result_list);
    if (result == 0)
    {
        memcpy(pAddr, result_list->ai_addr, result_list->ai_addrlen);
        freeaddrinfo(result_list);
    }
    return result;
}
//
