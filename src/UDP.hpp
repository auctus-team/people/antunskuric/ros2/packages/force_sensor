#ifndef UDP_HPP_INCLUDED
#define UDP_HPP_INCLUDED


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include <iostream>
#include <string>
#include <vector>
#include <sstream>


/*! \class UDPsocket
 *  \brief Helper class to abstract the linux socket creation painfull process
 *
 *  The class create a soket listening at specified port (constructor), and send message to local or distant adress trough specified port (may be different to the listning one)
 *
 */
class UDPsocket
{
    public:
        /// Default
        /*!
         *  \brief Constructor
         *  \param port : the listening port
         */
        UDPsocket(const int& port = 5013);
        
        /*!
         *  \brief Destructor
         */
        ~UDPsocket();
        //

        /// Communication functions
        /*!
         *  \brief Send a message
         *
         *  Send a message (datagram) of specified size to an host at a specified port.
         *
         *  \param msg : a pointer on the message to send
         *  \param msgSize : the message size
         *  \param destination : the destination adress (ex : "192.168.0.10")
         *  \param port : the destination port (ex : "5012")
         *  \return true if successfully sended, false otherwise. The returned value doesn't indicate if the message arrived correctly to host (it's UDP ...)
         */
        bool sendMessageTo(const uint8_t* msg, const unsigned short& msgSize, const char* destination, const char* port);
        
        /*!
         *  \brief Read if a message arrived
         *
         *  Read the last datagram in buffer, or return an empty message if an error occur.
         *
         *  \return the readed message (as a dynamic array of bytes)
         */
        std::vector<uint8_t> read();
        //

        /// Set / get functions
        /*!
         *  \brief Search the ip adress of a specified hostname
         *
         *  \param hostname : the hostname to find
         *  \param ip : the host ip adress
         *  \param lastState : an optionnal int to indicate the last check state (to avoid redundant logs) : 0=unspecified, 1=last succed, -1=last failed
         *  \return a boolean indicating if succeed or not to found host
         */
        static bool getIpFromHostname(const std::string& hostname, std::string* ip, const int& lastState = 0);
        
        /*!
         *  \brief Get the number of errors that occur from the program start
         *  \return the number of errors
         */
        unsigned long getErrorsCount() const;
        
        /*!
         *  \brief Get the last timestamp
         *
         *  The timestamp value is updated at each succesfull read and each successfull send
         *
         *  \return the last timestamp
         */
        unsigned long getTimestamp() const;
        //

    protected:
        /// Attributes
        int udpsocket;              //!< The UDP socket file descriptor
        unsigned long timestamp;    //!< Timestamp
        unsigned long errors;       //!< Number of errors
        uint8_t* rcvBuffer;         //!< Receive buffer (used internally)
        //

        /// Protected functions
		/*!
         *  \brief Compute the internal linux IP adress representation (IP4 or IPV6)
		 *  \param destination : the destination adress (ex : "192.168.0.10")
		 *  \param family : the protocol : has to be AF_INET for IPV4 or AF_INET6 for IPV6
		 *  \param port : the destination port (ex : "5012")
		 *  \param pAddr : the resulting weird adress representation
         *  \return 0 in case of success
         */
        int resolveDestination(const char* destination, int family, const char* port, sockaddr_storage* pAddr);
        //
};

#endif // UDP_HPP_INCLUDED
