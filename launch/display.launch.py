from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    config_rviz = os.path.join(
        get_package_share_directory('force_sensor'),
        'launch',
        'rviz.rviz'
        )

    config_yaml = os.path.join(
        get_package_share_directory('force_sensor'),
        'config',
        'ft.yaml'
        )

    return LaunchDescription([
        Node(
            package='force_sensor',
            executable='force_sensor_node',
            name='force_sensor_node',
            parameters=[config_yaml]
        ),
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', config_rviz]
        )
    ])